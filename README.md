<h1 align="center">
    Material UI Dashboard for Gatsby 🔥
</h1>

![Image description](./screenshot.png)

## 🚀 Quick start

1.  **Create a Gatsby site.**

    Use the Gatsby CLI to create a new site, specifying the blog starter.

    ```sh
    # create a new Gatsby site using starters
    gatsby new project-name https://github.com/git-user-name/gatsby-starter-project-name
    ```

1.  **Start developing.**

    Navigate into your new site’s directory and start it up.

    ```sh
    cd my-admin-tool-starter/
    gatsby develop
    ```

1.  **Open the source code and start editing!**

    Your site is now running at `http://localhost:8000`!

    _Note: You'll also see a second link: _`http://localhost:8000/___graphql`_. This is a tool you can use to experiment with querying your data. Learn more about using this tool in the [Gatsby tutorial](https://www.gatsbyjs.org/tutorial/part-five/#introducing-graphiql)._

    Open the `my-admin-tool-starter` directory in your code editor of choice and edit `src/pages/index.js`. Save your changes and the browser will update in real time!

---
### Thank you, @merantau